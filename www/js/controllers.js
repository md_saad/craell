angular.module('starter.controllers', [])

                                                                                                                      //App Ctrl
.controller('AppCtrl', function($scope, $ionicModal, $timeout, $ionicSideMenuDelegate) {

  $scope.openModal = function(id){
      $ionicModal.fromTemplateUrl(id, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal){
        modal.show();
        $scope.modal = modal;
      });
    };
    $scope.closeModal = function(){
      $scope.modal.hide();
    }
    
    $scope.$on('$destroy', function(){
      //$scope.modal.remove();
    });

    $scope.$on('modal.hidden', function(){

    });

    $scope.$on('modal.removed', function(){

    });

  $scope.toggleLeftSideMenu = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };


})

.controller('StartscreenCtrl', function($scope, $ionicModal) {

})
                                                                                                                      

                                                                                                                      //Home Ctrl
.controller('HomeCtrl', function($scope, $ionicModal, $http, $ionicSlideBoxDelegate) {

  $('.home-carasol').owlCarousel({
          loop:true,
          margin:0,
          
          responsive:{
              0:{
                  items:1
              },
              768:{
                  items:2
              },
              1024:{
                  items:2
              }
          }
  });



  $scope.homeData;
  var sliderData = [];
  var tempData = {};
  var p=0;
  var nowTime =Date.now();

  $scope.run = function(tempimage){
    $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+tempimage[0],  {
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }, 'cr_time='+nowTime).success(function(response){

                      tempData.image = response.image_url;
                      tempData.link = $scope.homeData["main_slider_"+p+"_link"][0];
                      tempData.text = $scope.homeData["main_slider_"+p+"_text"][0];
                     
                      sliderData.push(tempData);

               
                      tempData ={};
                      $ionicSlideBoxDelegate.update();
                    })
                    .error(function(response){

                    })
  }


  $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=page_info', {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(response){
    $scope.homeData = response.page_details;
    for(var count=0; count<$scope.homeData.length; count++){
      if($scope.homeData[count].id == 10){
        $scope.homeData = $scope.homeData[count].all_meta;
        var imageid = $scope.homeData.item_list_0_image[0];
        var imageid2 = $scope.homeData.item_list_2_image[0];
        var imageid3 = $scope.homeData.item_list_0_image_bg[0];
        var imageid4 = $scope.homeData.item_list_2_image_bg[0];
        var imageid5 = $scope.homeData.image_5[0];
        var imageid6 = $scope.homeData.image_3[0];

 
          $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+imageid,  {
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            $scope.homeData.item_list_0_image[0] = response.image_url;
            
          })
          .error(function(){

          })
          $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+imageid2,  {
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            $scope.homeData.item_list_2_image[0] = response.image_url;
            
          })
          .error(function(){

          })
          $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+imageid3,  {
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            $scope.homeData.item_list_0_image_bg[0] = response.image_url;
            
          })
          .error(function(){

          })
          $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+imageid4,  {
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            $scope.homeData.item_list_2_image_bg[0] = response.image_url;
           
          })
          .error(function(){

          })
          $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+imageid5,  {
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            $scope.homeData.image_5[0] = response.image_url;
            
          })
          .error(function(){

          })
          $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+imageid6,  {
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            $scope.homeData.image_3[0] = response.image_url;
            
          })
          .error(function(){

          })


          for (var p=0; p<6; p++) {
              var tempimage = $scope.homeData["main_slider_"+p+"_image"];
              if (tempimage!=undefined) {
                    $scope.run(tempimage);
                    
              }
              else{

                break;
              }     
          };

        break; 
      }
    }
  })
  .error(function(response){
    
  })


  $scope.returnhomeData = function(){
    return $scope.homeData;
  }

  $scope.returnsliderData = function(){
    return sliderData;
  }



  $scope.returnProductData = function(){
    return $scope.productData;
  }

  $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=product_info', {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(response){
    $scope.productData = response.page_details;
  
    $ionicSlideBoxDelegate.update();
  })
  .error(function(){
    $scope.productData = null;
  })

 $scope.footerData;
  $scope.returnfooterData = function(){
    return $scope.footerData;
  }

  $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=footer_info', {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(response){
    $scope.footerData = response.footer;
  })
  .error(function(response){
    
  })
})
                                                                                                          
                                                                                                                //About Us Ctrl
.controller('AboutusCtrl', function($scope, $http, $ionicSlideBoxDelegate) {


  $scope.aboutData;
  $scope.aboutArray =[];

  var nowTime =Date.now();
  var sliderData = [];
  var tempData = {};
  var p=0;
  $scope.run = function(tempimage){
    $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+tempimage[0],  {
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function(response){

                      tempData.image = response.image_url;
                      tempData.name = $scope.aboutData["item_list_"+p+"_title_name"][0];
                      tempData.text = $scope.aboutData["item_list_"+p+"_title_text"][0];
                    
                      sliderData.push(tempData);

                
                      tempData ={};
                      $ionicSlideBoxDelegate.update();
                    })
                    .error(function(response){

                    })
  }

  $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=page_info', {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(response){
    
    $scope.aboutData = response.page_details;
    $.ionTabs("#tabs_2",{
      type: "none"
    });

    for(var count=0; count<$scope.aboutData.length; count++){
      if($scope.aboutData[count].id == 12){
        $scope.aboutData = $scope.aboutData[count].all_meta;
        $scope.aboutArray[0]=$scope.aboutData;
        var imageid = $scope.aboutArray[0].banner[0];
        var imageid2 = $scope.aboutArray[0].image_2[0];
        var imageid3 = $scope.aboutArray[0].image_left[0];
        var imageid4 = $scope.aboutArray[0].image_right[0];

          $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+imageid,  {
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            $scope.aboutArray[0].banner[0] = response.image_url;
          })
          .error(function(){

          })

          $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+imageid2,  {
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            $scope.aboutArray[0].image_2[0] = response.image_url;

          })
          .error(function(){

          })

          $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+imageid3,  {
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            $scope.aboutArray[0].image_left[0] = response.image_url;
          })
          .error(function(){

          })

          $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+imageid3,  {
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            $scope.aboutArray[0].image_right[0] = response.image_url;

          })
          .error(function(){

          })

          for (var p=0; p<6; p++) {
              var tempimage = $scope.aboutData["item_list_"+p+"_image"];
              if (tempimage!=undefined) {
                    $scope.run(tempimage);
                    
              }
              else{

                break;
              }     
          };

        break; 
      }
    }

  })
  .error(function(response){
    
  })


  $scope.returnaboutData = function(){
    return $scope.aboutArray[0];
  }

  $scope.footerData;
  $scope.returnfooterData = function(){
    return $scope.footerData;
  }

  $scope.returnsliderData = function(){
    return sliderData;
  }

  $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=footer_info', {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(response){
    $scope.footerData = response.footer;
  })
  .error(function(response){
    
  })
})
                                                                                                                
                                                                                                                //Services Ctrl
.controller('ServicesCtrl', function($scope, $http, $ionicSlideBoxDelegate) {

  
  $('#click_advance').click(function(){
      $('#display_advance').toggle('1000');
      $("i",this).toggleClass("ion-arrow-up-b");
    });


  $scope.accordianHeader = "Services Loren Ipsum 01";
  $scope.selectedIndex = 0;

  $scope.passSelect = function(name, index){
    $scope.accordianHeader = name;
    $scope.selectedIndex = index;
  }

  $scope.serviceData;
  $scope.serviceArray = [];
  $scope.benefitsArray = [];
  $scope.featuressArray = [];

  var nowTime =Date.now();

  $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=page_info', {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(response){
    
    $scope.serviceData = response.page_details;
    $.ionTabs("#tabs_1", {
      type: "none"
    });

    for(var count=0; count<$scope.serviceData.length; count++){
      if($scope.serviceData[count].id == 157){
        $scope.serviceData = $scope.serviceData[count].all_meta;
        $scope.serviceArray[0]=$scope.serviceData;
      
        var imageid = $scope.serviceArray[0].banner[0];
        var imageid2 = $scope.serviceArray[0].image_2[0];
 
        $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+imageid,  {
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            $scope.serviceArray[0].banner[0] = response.image_url;
           
          })
          .error(function(){

          })

         $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+imageid2,  {
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            $scope.serviceArray[0].image_2[0] = response.image_url;
           
          })
          .error(function(){

          })

          for(var i=0; i<10; i++){
            if ($scope.serviceData["item_list_3_"+i+"_title"]!=undefined) {
              var data = {};
              data.title = $scope.serviceData["item_list_3_"+i+"_title"][0];
              data.content = $scope.serviceData["item_list_3_"+i+"_content"][0];

              $scope.benefitsArray.push(data);
              
            }
            else{
              break;
            }  
          }

          for(var i=0; i<10; i++){
            if ($scope.serviceData["item_list_4_"+i+"_title"]!=undefined) {
              var data = {};
              data.title = $scope.serviceData["item_list_4_"+i+"_title"][0];
              data.content = $scope.serviceData["item_list_4_"+i+"_content"][0];

              $scope.featuressArray.push(data);
              
            }
            else{
              break;
            }  
          }

        break; 
      }
    }

  })
  .error(function(response){
    
  })


  $scope.returnserviceData = function(){
    return $scope.serviceArray[0];
  }

  $scope.returnbenefitsArray = function(){
    return $scope.benefitsArray;
  }

  $scope.returnfeaturessArray = function(){
    return $scope.featuressArray;
  }

  $scope.footerData;
  $scope.returnfooterData = function(){
    return $scope.footerData;
  }

  $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=footer_info', {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(response){
    $scope.footerData = response.footer;

  })
  .error(function(response){
    
  })
})

                                                                                                          //Projects Ctrl
.controller('ProjectsCtrl', function($scope, $http) {

  var nowTime =Date.now();

  $scope.projectsData;
  $scope.projectsArray = [];
  $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=page_info', {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(response){
    $scope.projectsData = response.page_details;
    for(var count=0; count<$scope.projectsData.length; count++){
      if($scope.projectsData[count].id == 16){
        $scope.projectsData = $scope.projectsData[count].all_meta;
        $scope.projectsArray[0]=$scope.projectsData;
        var imageid = $scope.projectsArray[0].banner[0];
        $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+imageid,  {
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            $scope.projectsArray[0].banner[0] = response.image_url;

          })
          .error(function(){

          })
      }
    }
  })
  .error(function(){

  })     

  $scope.returnprojectsData = function(){
    return $scope.projectsData;
  }
  $scope.returnprojectsArray = function(){
    return $scope.projectsArray;
  }

  $scope.footerData;
  $scope.returnfooterData = function(){
    return $scope.footerData;
  }

  $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=footer_info', {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(response){
    $scope.footerData = response.footer;

  })
  .error(function(response){
    
  })
})

                                                                                                          //Resources Ctrl
.controller('ResourcesCtrl', function($scope, $http, $ionicSlideBoxDelegate) {

    var nowTime =Date.now();
    $('#click_advance').click(function(){
      $('#display_advance').toggle('1000');
      $("i",this).toggleClass("ion-arrow-up-b");
    });

  $scope.accordianHeader;
  $scope.selectedIndex = 0;


  $scope.resourcesData = {};
  $scope.resourcesArray = [];
  $scope.test;
  $scope.test2;

  $scope.passSelect = function(name, index){
    $scope.accordianHeader = name;
    $scope.selectedIndex = index;

    for(var i=0;i<$scope.test.length;i++){

      if ($scope.test[i].name== name) {
        
        $scope.test2= $scope.test[i].posts;
       
        break;
      }
      

    }
  }  


  $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=resource_banner', {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(response){
    $scope.test2= response.all_info[0].posts;
    $scope.test= response.all_info;
    $scope.accordianHeader = response.all_info[0].name;
    $scope.resourcesData.posts = response.all_info[0].posts;
    $scope.resourcesData.name = $scope.accordianHeader;
    $scope.resourcesArray.push($scope.resourcesData);

     

    /*$http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+response.all_info.banner,  {
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            response.all_info.banner = response.image_url;
            
          })
          .error(function(){

          })*/
  })
  .error(function(){

  })  

  
  $scope.returnresourcesData = function(){
    
    return $scope.test;
  }

  $scope.returnresourcesArray = function(){
    return $scope.resourcesArray;
  }

  $scope.footerData;
  $scope.returnfooterData = function(){
    return $scope.footerData;
  }

  $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=footer_info', {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(response){
    $scope.footerData = response.footer;
  })
  .error(function(response){
    
  })
})

                                                                                                                //Contact Us Ctrl
.controller('ContactusCtrl', function($scope, $http, $ionicSlideBoxDelegate, $ionicPopup) {



  var nowTime =Date.now();
  $scope.contactusData;
  $scope.contactusArray = [];
  $scope.contactsarray = [];
  $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=page_info', {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(response){
    $scope.contactusData = response.page_details;
    $.ionTabs("#tabs_3", {
      type: "none"
    });
    for(var count=0; count<$scope.contactusData.length; count++){
      if($scope.contactusData[count].id == 20){
        $scope.contactusData = $scope.contactusData[count].all_meta;
        $scope.contactusArray[0]=$scope.contactusData;
        var imageid = $scope.contactusArray[0].banner[0];
        $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+imageid,  {
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            $scope.contactusArray[0].banner[0] = response.image_url;

          })
          .error(function(){

          })

        for(var i=0; i<10; i++){

            if ($scope.contactusData["item_right_"+i+"_title"]!=undefined) {
              var data = {};
              data.title = $scope.contactusData["item_right_"+i+"_title"][0];
              data.text = $scope.contactusData["item_right_"+i+"_left_text"][0];
              data.telephone = $scope.contactusData["item_right_"+i+"_telephone"][0];
              data.email = $scope.contactusData["item_right_"+i+"_email"][0];
              $scope.contactsarray.push(data);

              $scope.updateContacts($scope.contactsarray);
            }
            else{
              break;
            }  
          }  
      }
    }
  })
  .error(function(){

  })     

  $scope.returnrecontactusData = function(){
    return $scope.contactusData;
  }

  $scope.returnrecontactusArray = function(){
    return $scope.contactusArray;
  }

  $scope.returnrecontactsArray = function(){
    return $scope.contactsarray;
  }

  $scope.groups = [];

  $scope.updateContacts = function(x){
 
    for (var i=0; i<x.length; i++) {
      $scope.groups[i] = {
        name: x[i].title,
        items: []
      };
      for (var j=0; j<1; j++) {
        $scope.groups[i].items.push({location: x[i].text, email: x[i].email, contact: x[i].telephone});
      }
    }
  }
  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };

  $scope.txt = {};

  $scope.sendMail = function(txt){
    $scope.body = "Name: "+txt.name+"\n"+"Email: "+txt.email+"\n"+"Contact: "+txt.contact+"\n"+"Message: "+txt.message;
    if(window.plugins && window.plugins.emailComposer) {
              window.plugins.emailComposer.showEmailComposerWithCallback(function(result) {
                  $scope.txt = {};
              }, 
              txt.subject, // Subject
              $scope.body,                      // Body
              [$scope.mailaddress],    // To
              null,                    // CC
              null,                    // BCC
              false,                   // isHTML
              null,                    // Attachments
              null);                   // Attachment Data
    }
  }

  $scope.footerData;
  $scope.returnfooterData = function(){
    return $scope.footerData;
  }

  $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=footer_info', {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(response){
    $scope.footerData = response.footer;

  })
  .error(function(response){
    
  })
})
                                                                                                                  
                                                                                                                  //Careers Ctrl
.controller('CareersCtrl', function($scope, $http, $ionicSlideBoxDelegate) {

  var nowTime =Date.now();
  $scope.careersData;
  $scope.careersArray = [];
  $scope.jobssArray = [];


  $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=page_info', {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(response){
    $scope.careersData = response.page_details;
    for(var count=0; count<$scope.careersData.length; count++){
      if($scope.careersData[count].id == 22){
        $scope.careersData = $scope.careersData[count].all_meta;
        $scope.careersArray[0]=$scope.careersData;
        

        document.getElementById('emailctrl').innerHTML = $scope.careersData.email[0];

        var imageid = $scope.careersArray[0].banner[0];
 
        $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=image_url&image_id='+imageid,  {
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response){
            
            $scope.careersArray[0].banner[0] = response.image_url;
            
          })
          .error(function(){

          })


          for(var i=0; i<10; i++){
            if ($scope.careersData["list_item_"+i+"_link"]!=undefined) {
              var data = {};
              data.link = $scope.careersData["list_item_"+0+"_link"][0];
              data.text = $scope.careersData["list_item_"+0+"_text"][0];
              data.description = $scope.careersData["list_item_"+0+"_description"][0];

              $scope.jobssArray.push(data);
    
            }
            else{
              break;
            }  
          }

        break; 
      }
    }

  })
  .error(function(response){
    
  })


  $scope.returncareersData = function(){
    return $scope.careersArray[0];
  }

  $scope.returnjobsArray = function(){
    return $scope.jobssArray;
  }

  $scope.footerData;
  $scope.returnfooterData = function(){
    return $scope.footerData;
  }

  $http.post('http://craell.com/iosapi?cr_time='+nowTime, 'action=footer_info', {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(response){
    $scope.footerData = response.footer;

  })
  .error(function(response){
    
  })

});
