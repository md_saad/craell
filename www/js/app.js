// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/',
    abstract: true,
    templateUrl: 'index.html',
    controller: 'AppCtrl'
  })

  .state('start_screen', {
    url: '/start_screen',
    views: {
      'start': {
      templateUrl: 'templates/start_screen.html',
      controller: 'StartscreenCtrl'
      }
    }  
  })

  .state('home', {
    url: '/home',
    views: {
      'craell': {
      templateUrl: 'templates/home.html',
      controller: 'HomeCtrl'
      }
    }  
  })

  .state('aboutus', {
    url: '/aboutus',
    views: {
      'craell': {
      templateUrl: 'templates/aboutus.html',
      controller: 'AboutusCtrl'
      }
    }  
  })

  .state('services', {
    url: '/services',
    views: {
      'craell': {
      templateUrl: 'templates/services.html',
      controller: 'ServicesCtrl'
      }
    }  
  })

  .state('projects', {
    url: '/projects',
    views: {
      'craell': {
      templateUrl: 'templates/projects.html',
      controller: 'ProjectsCtrl'
      }
    }  
  })

  .state('resources', {
    url: '/resources',
    views: {
      'craell': {
      templateUrl: 'templates/resources.html',
      controller: 'ResourcesCtrl'
      }
    }  
  })

  .state('contactus', {
    url: '/contactus',
    views: {
      'craell': {
      templateUrl: 'templates/contactus.html',
      controller: 'ContactusCtrl'
      }
    }  
  })

  .state('careers', {
    url: '/careers',
    views: {
      'craell': {
      templateUrl: 'templates/careers.html',
      controller: 'CareersCtrl'
      }
    }  
  })

  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/home');
});

